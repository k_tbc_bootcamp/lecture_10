package com.lkakulia.lecture_10

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.service.voice.VoiceInteractionSession
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_inputaddress.*

class InputAddressActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inputaddress)

        //initialize function call
        init()
    }

    //initialize function
    private fun init() {

        //send address button
        sendAddressButton.setOnClickListener({
            if (addressEditText.text.isEmpty()) {
                toastMessage("Please fill in the field")
            }
            else {
                val intent = Intent(this, RegisterActivity::class.java)
                intent.putExtra("address", addressEditText.text.toString())
                setResult(Activity.RESULT_OK, intent)

                finish()
            }
        })
    }

    //toast message function
    private fun toastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
