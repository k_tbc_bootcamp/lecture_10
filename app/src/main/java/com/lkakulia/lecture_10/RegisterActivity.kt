package com.lkakulia.lecture_10

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private val requestCodeAddressActivity = 999

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        //initialize function call
        init()
    }

    //initialize function
    private fun init() {

        //input page button listener
        inputPageButton.setOnClickListener({
            val intent = Intent(this, InputAddressActivity::class.java)
            startActivityForResult(intent, requestCodeAddressActivity)
        })

        //sign up button listener
        signUpButton.setOnClickListener {
            val username = usernameEditText.text.toString()
            val firstname = firstnameEditText.text.toString()
            val lastname = lastnameEditText.text.toString()
            val age = ageEditText.text.toString()
            val address = addressTextView.text.toString()

            //check for empty fields
            if (username.isEmpty() || firstname.isEmpty() ||
                lastname.isEmpty() || age.isEmpty() ||
                address == "Address to be input") {
                toastMessage("Please fill in all the fields")
            }
            else {
                val user = UserModel(username, firstname, lastname, age, address)
                val intent = Intent(this, DashboardActivity::class.java)
                intent.putExtra("userModel", user)

                finish()
                startActivity(intent)
            }
        }
    }

    //toast message function
    private fun toastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    //activity result function
    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)

        if (requestCode == requestCodeAddressActivity && resultCode == Activity.RESULT_OK) {
            val address = intent!!.extras!!["address"].toString()

            addressTextView.text = address
            addressTextView.isAllCaps = false
        }
    }

}
