package com.lkakulia.lecture_10

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        //initialize function call
        init()
    }

    //initialize function
    private fun init() {

        //get UserModel object
        val user = intent.getParcelableExtra<UserModel>("userModel")

        //assign the values to the text views
        usernameTextView.text = "Username: ${user.username}"
        firstnameTextView.text = "Firstname: ${user.firstname}"
        lastnameTextView.text = "Lastname: ${user.lastname}"
        ageTextView.text = "Age: ${user.age}"
        addressTextView.text = "Address: ${user.address}"
    }
}
